
function mapObject(objectData,cb){
    if(typeof(objectData) === "object" && objectData != null && !(Array.isArray(objectData))){

        
        const objKeys = Object.keys(objectData);
        if(objKeys.length === 0){
            return ("Data not present");
        }

        const copy={};
        for(let index in objectData){
            copy[index]=cb(objectData[index],index);
        }

        return copy;
    }
    else{
        return ("Data is not an Object")
    }
}


module.exports=mapObject;