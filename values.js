
function values(objectData){
    if(typeof(objectData) === "object" && objectData != null && !(Array.isArray(objectData))){

        
        const objKeys = Object.keys(objectData);
        if(objKeys.length === 0){
            return ("Data not present");
        }

        const keysArray=[];

        for(let key in objectData){
            keysArray.push(objectData[key]);
        }

        return keysArray;

    }
    else{
        return ("Data is not an Object")
    }
}

module.exports=values;