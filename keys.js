
function keys(objectData){
    if(typeof(objectData) === "object" && objectData != null && !(Array.isArray(objectData))){

        const keysArray=[];

        for(let key in objectData){
            keysArray.push(key);
        }

        return keysArray;

    }
    else{
        return ("Data is not an Object")
    }
}

module.exports=keys;