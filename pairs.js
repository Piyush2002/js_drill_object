
function pairs(objectData){
    if(typeof(objectData) === "object" && objectData != null && !(Array.isArray(objectData))){

        
        const objKeys = Object.keys(objectData);
        if(objKeys.length === 0){
            return ("Data not present");
        }

        const store=[];

        for(let key in objectData){
            store.push([key,objectData[key]]);
        }

        return store;
    }
    else{
        return ("Data is not an Object")
    }
    
}

module.exports=pairs;